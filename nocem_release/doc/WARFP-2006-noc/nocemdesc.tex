\section{NoCem Description}

%\begin{figure}
%\centering
%\includegraphics{noc2proc.eps}
%\caption{Block Diagram of Processors Connected to NoCem}
%\label{fig:noc2proc}
%\end{figure}

\textbf {NoCem design goals:} NoCem conforms to several goals in creating a tool that can emulate a variety of NoC configurations that is easily attached to current soft and embedded processors.  These goals and how they are met are briefly enumerated here.
\begin{itemize}
\item Ease of configuration.  This is done using VHDL generics and identifying what configurations are useful.  NoCem currently allows configurations in terms of data width, network topology, channel FIFO depth, and packet length among other derivative parameters.

\item Modularity of design.  The NoC itself is modularized into channels and nodes.  The nodes consist of a switching fabric and arbitration blocks.  These blocks conform to simple interfaces and be swapped out with more complex internals if needed.  

\item Common External Interface.  Each access point into the NoC has a similar interface regardless of the network internals.  This will allow easy integration of the NoC into existing tools to connect FPGA logic to the processors.
\end{itemize}

\textbf{NoCem Interface:} The NoCem interface is very similar to that of a FIFO.  Depending on the configuration of the NoC, the same lines are used for different purposes.  NoCem does have a bus network configuration, though the standard Xilinx bus architectures are presumably more complete and EDK provides better tools to use them.  The interface for both egress and ingress paths contain:
\begin{itemize}
\item \textit{data path:}  Datalines are connected to the channel FIFO for the access point into the NoC.  Data is read or written using standard FIFO read and write enables.
\item \textit{packet control path:} Packet control is written to the channel FIFO for the access point into the NoC.  Packet control content is dependent on the network itself.  Typically, it will contain the packet header information such as source address, destination address, packet length, and other metadata.  This information is used by the internal network to route the packet.  NoCem currently supports destination based deterministic routing, but this can be extended in future versions.  The only current restriction is the necessity for a destination address in the first word of the packet control.  This restriction allows for fast routing, but NoCem could be extended to overcome this limitation.
\item \textit{channel metadata path:} These lines are used to arbitrate and grant access to the NoC by an access point.  In the simplest case, these lines consist of various FIFO status signals (e.g. almost empty/full thresholds).
\end{itemize}

This interface hides any underlying network configurations that may exist.  Only modifying the datawidth or packet control structure will require outside modifications to interface with NoCem.  


\textbf{Connection to Processors:} NoCem is intended for Xilinx FPGAs and provides access point bridges (noc2proc\_$<$bridge\_name$>$) to both the PLB and OPB buses.  These buses then connect to the embedded PowerPC and soft Microblaze processors.  These bridges are easily integrated into Xilinx's EDK software, where the processors can be instantiated and connected to the NoC.  Bus to NoC communication and vice versa is done using simple memory mapping of addresses which is the standard for Xilinx EDK projects.  Figure~\ref{fig:noc2proc} shows a block diagram of this bridging.  An accompanying software driver is used to read and write the NoC from the processor.  For sending a packet, the processor notifies the bridge of the packet destination and length.  The processor can then write the packet into the NoC.  For receiving a packet, the bridge interrupts the processor and allows the processor driver to ``upload'' the packet into the processor's memory space.  A more complicated buffering and aggregation scheme could be added to NoCem, only requiring modification of the bridges themselves.   


\begin{figure}
\centering
\includegraphics{node.eps}
\caption{Modular design of NoC nodes.  Each node has an access point and a north, south, east, and west channel FIFO.}
\label{fig:node}
\end{figure}

The noc2proc bridges can handle both the PLB and OPB datawidths.  Both buses have a 32-bit address space, but the OPB and PLB have a 32-bit and 64-bit datawidth respectively.  Since both use Xilinx's IPIF (IP Interface), this results in only minor differences in actual code.




\textbf{Implementation:} NoCem is implemented in standard VHDL with heavy use of generics throughout the code base  To use with Xilinx's soft and embedded processors, we integrate NoCem and the accompanying noc2proc bridges into an EDK project.  Using an EDK project requires some generation of files and metadata, but we use scripts wherever possible.  An instantiation of NoCem can be used natively in any project as well, bypassing the scripts and bridges.   However, for multiprocessor systems, integration into an EDK project is often a necessity.  NoCem was created and simulated using Xilinx ISE 7.1.4, Xilinx EDK 7.1.2, and Mentor Graphics Modelsim 6.1.



NoCem was implemented with extensibility in mind.  Figure~\ref{fig:node} shows the block diagram of a common node in any Network on Chip.  Typically there are 5 entry points into the node (North, South, West, East, Access Point).  We did not consider other node configurations (e.g. hexagonal networks have previously been suggested for parallel processing~\cite{361824}).  The channel FIFOs transmit and receive data and metadata to and from the node.  How and what is transmitted is modularized to involve only the arbiter and the channel FIFO's channel\_metadata signals (not shown in figure).  The switch itself is an all-to-all mux that allows multiple paths of communication simultaneously.  Any of these components can be switched out to add various NoC concepts (virtual channels, QoS, reservation systems) without affecting the remainder of the system.  

The noc2proc bridges are stored as a Xilinx EDK peripheral, which consists of a variety of code and metadata files.  Even though the bridges share almost identical code sources, they are kept separate for easy integration into EDK projects.  Accompanying software drivers have been created to do simple packet receiving and transmitting.

\begin{table}
\centering
\caption{Number of LUTs used by NoCem.}
\label{tab:size}
\begin{tabular}{|p{.7in}|p{.7in}|p{.7in}|p{.7in}|} 
\hline
NoC Dimensions & Datawidth & LUTs & xc2vp30 LUTs used(\%) \\ \hline
2x2 & 16b  & 4,086 & 14\%  \\  \hline
3x3 & 16b & 11,693 & 42\%  \\ \hline
4x4 & 16b & 21,570 & 78\%  \\ \hline
2x2 & 32b & 5,822  & 21\% \\ \hline
3x3 & 32b & 16,394 & 59\% \\ \hline
4x4 & 32b & 34,370 & 125\% \\ \hline




\end{tabular}
\end{table}

\textbf{Performance and Functional Measurements:} NoCem supports buswidths up to 256b, but the larger the datawidth, the more FPGA fabric required to route wires and registers.  Table~\ref{tab:size} shows how for a 16b and 32b datawidth the size of NoCem increases.  For a Virtex-II Pro xc2vp30, the percentage of fabric used can be seen.  We chose to look at the xc2vp30, as it is the FPGA used in Digilent's XUP (Xilinx University Program) board~\cite{board-xupv2p} and Nallatech's XtremeDSP board~\cite{board-nallatech}.  These are two popular platform FPGAs and ones we possess.  Interestingly, a 4x4 mesh with 32b lines cannot fit on this part.  Of course, larger FPGAs can support this number of LUTs and even larger NoCs.  

Currently NoCem uses no onchip BRAMs for buffering purposes on channel FIFOs, which could be seen as a way to alleviate LUT usage.  This design decision was made for several reasons.  Primarily, we wanted to allow for BRAM to be used for processors' onchip caches allowing for that valuable resource to be used for various memory research.  Secondly, using BRAM for a channel FIFO is probably overkill, as each BRAM holds 2KB, there really is no need for that much in-network buffering on a per channel basis.  The Xilinx provided LUT-based FIFOs can be configured to a variety of lengths and widths.  This flexible FIFO structure allows for  more complete onchip interconnect experimentation.  For example, this flexibility in FIFO length could be used to confirm simulated NoC buffer sizes and its effect~\cite{881610}.  Delay is minimized using First-Word-Fallthrough FIFOs, which minimize the delay in valid data appearing on outputs of the channel FIFOs.

With all the configurations, the highest sustainable clockspeed is consistently 140-150 MHz.  This number would of course change depending on what is attached to NoCem.  The clock used by NoCem is typically driven from the PLB or OPB generated buses, which is part of the noc2proc bridge interface.

Some latency is added in the bridging between the NoC and the PLB or OPB buses.  While the bridging operations add a few cycles of delay in handshaking, it is unavoidable.  Bus arbitration with other peripherals competing for the bus may also add nondeterministic delays.  For this reason, extra bus-intensive peripherals should be avoided on buses communicating with NoCem if performance measurements are being taken involving the noc2proc bridge.  Memory operations for data and instructions are a notable example of this observation, but cannot be avoided in some configurations.  Various caching and alternative bus schemes can be used to alleviate these shared medium costs.  Specifically, the PowerPC and Microblaze processors allow caches tied directly to the processor (i.e. only using bus for cache misses and not cache hits), and other buses such as the OCM can be used for processor memory. 









