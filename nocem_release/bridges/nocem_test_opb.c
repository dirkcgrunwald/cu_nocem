// Located in: microblaze_0/include/xparameters.h
#include "xparameters.h"
#include "xbasic_types.h" 
#include "noc2proc_opb.h"
#include "xio.h"
//#include "xintc_l.h"
//#include "xexception_l.h"





int main (void) {

	Xuint32 pkt_metadata,pkt_data1,pkt_data2;

	Xuint32 pkt_dest,pkt_length,recvd_bytes;

	Xuint32 i,j;


	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	/*
		NOCEM RECV TESTING:
			1. enable interrupts correctly
			2. should then just be able to let NoCem run.....
	*/

/*
	// general init at PPC level
	XExc_Init();
	XExc_RegisterHandler(XEXC_ID_NON_CRITICAL_INT,(XExceptionHandler)XIntc_DeviceInterruptHandler, (void *)0);


	// register handlers associated with interrupt controller
	//XIntc_RegisterHandler(XPAR_OPB_INTC_0_BASEADDR,XPAR_OPB_INTC_0_RS232_UART_1_IP2INTC_IRPT_INTR, ??????, (void *)0);
	XIntc_RegisterHandler(XPAR_OPB_INTC_0_BASEADDR,XPAR_OPB_INTC_0_NOC2PROC_PLB_0_IP2INTC_IRPT_INTR, NOC2PROC_PLB_Intr_DefaultHandler, (void *)0);


	// enable interrupt controller
  	XIntc_mMasterEnable(XPAR_OPB_INTC_0_BASEADDR);


	// enable interrupts at the CONTROLLER level
	XIntc_mEnableIntr(XPAR_OPB_INTC_0_BASEADDR, XPAR_NOC2PROC_OPB_0_IP2INTC_IRPT_MASK | XPAR_RS232_UART_1_IP2INTC_IRPT_MASK | XPAR_DDR_256MB_32MX64_RANK1_ROW13_COL10_CL2_5_IP2INTC_IRPT_MASK);


	// enable the noc2proc bridge
	NOC2PROC_PLB_EnableInterrupt(XPAR_NOC2PROC_OPB_0_BASEADDR);
	//UART_?????_EnableInterrupt();


	// allow exceptions to enter PPC
	XExc_mEnableExceptions(XEXC_NON_CRITICAL); 

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

*/

/*	
		NOCEM SEND TESTING:  SEND 3 PACKETS, one to each of the exercisers (2x2 grid, PPC is at
		location (1,1) #3 node.
*/	

	for (i=0;i<3;i++)
	{
		pkt_metadata = 0;
		pkt_dest = i;
		pkt_length = i+5;
		
		pkt_metadata |= (pkt_dest);
		pkt_metadata |= (pkt_length << 8);

		// send metadata
		XIo_Out32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x000, pkt_metadata);

		//send data
		pkt_data1 = 0x01020304;
		pkt_data2 = 0x05060708;

		XIo_Out32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x004, pkt_data1);
		XIo_Out32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x008, pkt_data2);			
	}
	

/*	
		NOCEM RECV TESTING:  Receive Packets for as long as wanted.  The NoC Access Exercisers
		are filling up packet buffer continually.
*/


	for (i=0;i<10;i++)
	{
		//pkt_metadata = XIo_In32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x1FC);
		pkt_metadata = XIo_In32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x080);
		pkt_length = ((pkt_metadata & 0x0000FF00) >> 8);

		if (pkt_length != 0)
		{
			recvd_bytes = 0;
			while(recvd_bytes < pkt_length)
			{
				//pkt_data1 = XIo_In32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x1CC+recvd_bytes);
				pkt_data1 = XIo_In32(XPAR_NOC2PROC_OPB_0_BASEADDR + 0x084+recvd_bytes);

				recvd_bytes += 4;
			}
		}
	}










   //print("-- Exiting main() --\r\n");
   return 0;
}

