//////////////////////////////////////////////////////////////////////////////
//
// ***************************************************************************
// **                                                                       **
// ** Copyright (c) 1995-2005 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** You may copy and modify these files for your own internal use solely  **
// ** with Xilinx programmable logic devices and Xilinx EDK system or       **
// ** create IP modules solely for Xilinx programmable logic devices and    **
// ** Xilinx EDK system. No rights are granted to distribute any files      **
// ** unless they are distributed in Xilinx programmable logic devices.     **
// **                                                                       **
// ***************************************************************************
//
//////////////////////////////////////////////////////////////////////////////
// Filename:          C:\repository\MyProcessorIPLib\drivers\noc2proc_plb_v1_00_a\src\noc2proc_plb_selftest.c
// Version:           1.00.a
// Description:       Contains a diagnostic self-test function for the noc2proc_plb driver
// Date:              Wed Jun 07 21:43:50 2006 (by Create and Import Peripheral Wizard)
//////////////////////////////////////////////////////////////////////////////


/***************************** Include Files *******************************/

#include "noc2proc_plb.h"

/************************** Constant Definitions ***************************/


/************************** Variable Definitions ****************************/


/************************** Function Definitions ***************************/

/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the NOC2PROC_PLB instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus NOC2PROC_PLB_SelfTest(void * baseaddr_p)
{
   int     Index;
   Xuint32 baseaddr;
   Xuint8  Reg8Value;
   Xuint16 Reg16Value;
   Xuint32 Reg32Value;
   Xuint64 Reg64Value;

   /*
    * Assert the argument
    */
   XASSERT_NONVOID(baseaddr_p != XNULL);
   baseaddr = (Xuint32) baseaddr_p;

   xil_printf("******************************\n\r");
   xil_printf("* User Peripheral Self Test\n\r");
   xil_printf("******************************\n\n\r");

   /*
    * Enable all possible interrupts and clear interrupt status register(s)
    */
   xil_printf("Interrupt controller test...\n\r");
   Reg32Value = NOC2PROC_PLB_mReadReg(baseaddr, NOC2PROC_PLB_INTR_ISR_OFFSET);
   xil_printf("   - IP (user logic) interrupt status : 0x%08x \n\r", Reg32Value);
   xil_printf("   - clear IP (user logic) interrupt status register\n\r");
   NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_ISR_OFFSET, Reg32Value);
   Reg32Value = NOC2PROC_PLB_mReadReg(baseaddr, NOC2PROC_PLB_INTR_DISR_OFFSET);
   xil_printf("   - Device (peripheral) interrupt status : 0x%08x \n\r", Reg32Value);
   xil_printf("   - clear Device (peripheral) interrupt status register\n\r");
   NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_DISR_OFFSET, Reg32Value);
   xil_printf("   - enable all possible interrupt(s)\n\r");
   NOC2PROC_PLB_EnableInterrupt(baseaddr_p);
   xil_printf("   - write/read interrupt register passed \n\n\r");

   return XST_SUCCESS;
}
