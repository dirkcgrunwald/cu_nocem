//////////////////////////////////////////////////////////////////////////////
//
// ***************************************************************************
// **                                                                       **
// ** Copyright (c) 1995-2005 Xilinx, Inc.  All rights reserved.            **
// **                                                                       **
// ** You may copy and modify these files for your own internal use solely  **
// ** with Xilinx programmable logic devices and Xilinx EDK system or       **
// ** create IP modules solely for Xilinx programmable logic devices and    **
// ** Xilinx EDK system. No rights are granted to distribute any files      **
// ** unless they are distributed in Xilinx programmable logic devices.     **
// **                                                                       **
// ***************************************************************************
//
//////////////////////////////////////////////////////////////////////////////
// Filename:          C:\repository\MyProcessorIPLib\drivers\noc2proc_plb_v1_00_a\src\noc2proc_plb.c
// Version:           1.00.a
// Description:       noc2proc_plb Driver Source File
// Date:              Wed Jun 07 21:43:50 2006 (by Create and Import Peripheral Wizard)
//////////////////////////////////////////////////////////////////////////////


/***************************** Include Files *******************************/

#include "noc2proc_plb.h"

/************************** Function Definitions ***************************/

/**
 *
 * Enable all possible interrupts from NOC2PROC_PLB device.
 *
 * @param   baseaddr_p is the base address of the NOC2PROC_PLB device.
 *
 * @return  None.
 *
 * @note    None.
 *
 */
void NOC2PROC_PLB_EnableInterrupt(void * baseaddr_p)
{
  Xuint32 baseaddr;
  baseaddr = (Xuint32) baseaddr_p;

  /*
   * Enable all interrupt source from user logic.
   */
  NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_IER_OFFSET, 0x00000001);

  /*
   * Enable all possible interrupt sources from device.
   */
  NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_DIER_OFFSET,
    INTR_TERR_MASK
    | INTR_DPTO_MASK
    | INTR_IPIR_MASK
    );

  /*
   * Set global interrupt enable.
   */
  NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_DGIER_OFFSET, INTR_GIE_MASK);
}

/**
 *
 * Example interrupt controller handler for NOC2PROC_PLB device.
 * This is to show example of how to toggle write back ISR to clear interrupts.
 *
 * @param   baseaddr_p is the base address of the NOC2PROC_PLB device.
 *
 * @return  None.
 *
 * @note    None.
 *
 */
void NOC2PROC_PLB_Intr_DefaultHandler(void * baseaddr_p)
{
  Xuint32 baseaddr;
  Xuint32 IntrStatus;

  baseaddr = (Xuint32) baseaddr_p;

  /*
   * Get status from IP Interrupt Status Register.
   */
  IntrStatus = NOC2PROC_PLB_mReadReg(baseaddr, NOC2PROC_PLB_INTR_ISR_OFFSET);

  xil_printf("IP Interrupt! Status register (ISR) value : 0x%08x \n\r", IntrStatus);

  /*
   * Clear IP interrupts by toggle write back to IP ISR register.
   */
  NOC2PROC_PLB_mWriteReg(baseaddr, NOC2PROC_PLB_INTR_ISR_OFFSET, IntrStatus);
}

