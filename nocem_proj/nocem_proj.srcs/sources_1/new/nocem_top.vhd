----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/10/2014 09:37:16 AM
-- Design Name: 
-- Module Name: nocem_top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--library UNISIM;
--use UNISIM.VComponents.all;
library work;
use work.pkg_nocem.all;

entity nocem_top is
   Port( 

		-- arbitration lines (usage depends on underlying network)
		arb_req             : in  std_logic_vector(8 downto 0);
		arb_cntrl_in_0      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_1      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_2      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_3      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_4      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_5      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_6      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_7      : in  std_logic_vector(7 downto 0);
        arb_cntrl_in_8      : in  std_logic_vector(7 downto 0);
   
		arb_grant           : out std_logic_vector(8 downto 0);
		arb_cntrl_out_0     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_1     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_2     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_3     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_4     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_5     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_6     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_7     : out  std_logic_vector(7 downto 0);
        arb_cntrl_out_8     : out  std_logic_vector(7 downto 0);

		--data and control incoming/outgoing line (usage depends on underlying network)
		datain_0            : in   std_logic_vector (31 downto 0);
	    datain_1            : in   std_logic_vector (31 downto 0);
        datain_2            : in   std_logic_vector (31 downto 0);		
		datain_3            : in   std_logic_vector (31 downto 0);
		datain_4            : in   std_logic_vector (31 downto 0);
		datain_5            : in   std_logic_vector (31 downto 0);
		datain_6            : in   std_logic_vector (31 downto 0);
		datain_7            : in   std_logic_vector (31 downto 0);
		datain_8            : in   std_logic_vector (31 downto 0);
				   
		datain_valid        : in   std_logic_vector(8 downto 0);
		datain_recvd        : out  std_logic_vector(8 downto 0);
   
		dataout_0           : out   std_logic_vector (31 downto 0);
	    dataout_1           : out   std_logic_vector (31 downto 0);
        dataout_2           : out   std_logic_vector (31 downto 0);		
		dataout_3           : out   std_logic_vector (31 downto 0);
		dataout_4           : out   std_logic_vector (31 downto 0);
		dataout_5           : out   std_logic_vector (31 downto 0);
		dataout_6           : out   std_logic_vector (31 downto 0);
		dataout_7           : out   std_logic_vector (31 downto 0);
		dataout_8           : out   std_logic_vector (31 downto 0);
		   
		dataout_valid       : out std_logic_vector(8 downto 0);
		dataout_recvd       : in  std_logic_vector(8 downto 0);
   
		pkt_cntrl_in_0      : in std_logic_vector (6 downto 0);
	    pkt_cntrl_in_1      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_2      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_3      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_4      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_5      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_6      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_7      : in std_logic_vector (6 downto 0);
        pkt_cntrl_in_8      : in std_logic_vector (6 downto 0);			
		
		pkt_cntrl_in_valid  : in   std_logic_vector(8 downto 0);
		pkt_cntrl_in_recvd  : out  std_logic_vector(8 downto 0);      
             
		pkt_cntrl_out_0     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_1     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_2     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_3     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_4     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_5     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_6     : out std_logic_vector (6 downto 0);
        pkt_cntrl_out_7     : out std_logic_vector (6 downto 0);             
        pkt_cntrl_out_8     : out std_logic_vector (6 downto 0);
        
		pkt_cntrl_out_valid : out std_logic_vector(8 downto 0);
		pkt_cntrl_out_recvd : in  std_logic_vector(8 downto 0);

		clk : in std_logic;
        rst : in std_logic
		
	);
end nocem_top;

architecture Behavioral of nocem_top is
component nocem Port( 

		-- arbitration lines (usage depends on underlying network)
		arb_req         : in  std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		arb_cntrl_in    : in  arb_cntrl_array(NOCEM_NUM_AP-1 downto 0);

		arb_grant         : out std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		arb_cntrl_out     : out  arb_cntrl_array(NOCEM_NUM_AP-1 downto 0);

		--data and control incoming/outgoing line (usage depends on underlying network)
		datain        : in   data_array(NOCEM_NUM_AP-1 downto 0);
		datain_valid  : in   std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		datain_recvd  : out  std_logic_vector(NOCEM_NUM_AP-1 downto 0);

		dataout       : out data_array(NOCEM_NUM_AP-1 downto 0);
		dataout_valid : out std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		dataout_recvd : in  std_logic_vector(NOCEM_NUM_AP-1 downto 0);

		pkt_cntrl_in        : in   pkt_cntrl_array(NOCEM_NUM_AP-1 downto 0);
		pkt_cntrl_in_valid  : in   std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		pkt_cntrl_in_recvd  : out  std_logic_vector(NOCEM_NUM_AP-1 downto 0);      
             
		pkt_cntrl_out       : out pkt_cntrl_array(NOCEM_NUM_AP-1 downto 0);
		pkt_cntrl_out_valid : out std_logic_vector(NOCEM_NUM_AP-1 downto 0);
		pkt_cntrl_out_recvd : in  std_logic_vector(NOCEM_NUM_AP-1 downto 0);

		clk : in std_logic;
      rst : in std_logic
		
	);
end component;
--
--
begin
-- Comp dec
    nocem_inst: nocem 
        port map(
         	arb_req => arb_req,        
         	arb_cntrl_in => (arb_cntrl_in_8,arb_cntrl_in_7,arb_cntrl_in_6,arb_cntrl_in_5,arb_cntrl_in_4,arb_cntrl_in_3,arb_cntrl_in_2,arb_cntrl_in_1,arb_cntrl_in_0),-- 
         	arb_grant => arb_grant,      
         	arb_cntrl_out(0) => arb_cntrl_out_0,
            arb_cntrl_out(1) => arb_cntrl_out_1,
            arb_cntrl_out(2) => arb_cntrl_out_2,
            arb_cntrl_out(3) => arb_cntrl_out_3,
            arb_cntrl_out(4) => arb_cntrl_out_4,
            arb_cntrl_out(5) => arb_cntrl_out_5,
            arb_cntrl_out(6) => arb_cntrl_out_6,
            arb_cntrl_out(7) => arb_cntrl_out_7,
            arb_cntrl_out(8) => arb_cntrl_out_8,          	
         	datain => (datain_8,datain_7,datain_6,datain_5,datain_4,datain_3,datain_2,datain_1,datain_0),--
         	datain_valid => datain_valid, 
         	datain_recvd => datain_recvd,  
         	dataout(0) => dataout_0,
         	dataout(1) => dataout_1,
         	dataout(2) => dataout_2,
         	dataout(3) => dataout_3,
         	dataout(4) => dataout_4,
         	dataout(5) => dataout_5,
         	dataout(6) => dataout_6,
         	dataout(7) => dataout_7,
         	dataout(8) => dataout_8,
         	dataout_valid => dataout_valid, 
         	dataout_recvd => dataout_recvd, 
         	pkt_cntrl_in => (pkt_cntrl_in_8,pkt_cntrl_in_7,pkt_cntrl_in_6,pkt_cntrl_in_5,pkt_cntrl_in_4,pkt_cntrl_in_3,pkt_cntrl_in_2,pkt_cntrl_in_1,pkt_cntrl_in_0),--   
         	pkt_cntrl_in_valid => pkt_cntrl_in_valid, 
         	pkt_cntrl_in_recvd => pkt_cntrl_in_recvd,  
         	pkt_cntrl_out(0) => pkt_cntrl_out_0,
         	pkt_cntrl_out(1) => pkt_cntrl_out_1,
         	pkt_cntrl_out(2) => pkt_cntrl_out_2,
         	pkt_cntrl_out(3) => pkt_cntrl_out_3,
         	pkt_cntrl_out(4) => pkt_cntrl_out_4,
         	pkt_cntrl_out(5) => pkt_cntrl_out_5,
         	pkt_cntrl_out(6) => pkt_cntrl_out_6,
         	pkt_cntrl_out(7) => pkt_cntrl_out_7,
         	pkt_cntrl_out(8) => pkt_cntrl_out_8,
         	pkt_cntrl_out_valid => pkt_cntrl_out_valid,
         	pkt_cntrl_out_recvd => pkt_cntrl_out_recvd,                        
         	clk => clk,     
            rst => rst   
         );

end Behavioral;
