
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity barrelshift4_wrapper is
    port(I0: in std_logic;
         I1: in std_logic;
         I2: in std_logic;
         I3: in std_logic;
         S0: in std_logic;
         S1: in std_logic;
         O3: out std_logic;
         O2: out std_logic;
         O1: out std_logic;
         O0: out std_logic);
end barrelshift4_wrapper;

architecture behavioral of barrelshift4_wrapper is
    signal input_signal   : std_logic_vector(3 downto 0);
    signal control_signal : std_logic_vector(1 downto 0);
    signal output_signal  : std_logic_vector(3 downto 0);
begin
    input_signal <= I3 & I2 & I1 & I0;
    control_signal <= S1 & S0;
    O3 <= output_signal(3);
    O2 <= output_signal(2);
    O1 <= output_signal(1);
    O0 <= output_signal(0);    
    process(input_signal) begin
        case control_signal is
            when "00" => output_signal <= input_signal(3 downto 0);
            when "01" => output_signal <= input_signal(2 downto 0) & input_signal(3);
            when "10" => output_signal <= input_signal(1 downto 0) & input_signal(3 downto 2);
            when "11" => output_signal <= input_signal(0) & input_signal(3 downto 1);
        end case;
     end process;
end;                     