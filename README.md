# README #


### What is this repository for? ###

* This is a version of the NoCem "Network On Chip" Emulator, which implements a Torus Routing Chip (TRC) network in VHDL, suitable for implementation on an FGPA.

### How do I get set up? ###

* TBD

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact